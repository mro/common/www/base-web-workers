# Base Web Workers

Some generic Web Workers

This library is developped according to our [guidelines](https://mro-dev.web.cern.ch/docs/drafts/current/en-smm-apc-web-guidelines.html).


# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

npm run build:prod
```

# Usage

WebPack [worker-plugin](https://github.com/GoogleChromeLabs/worker-plugin) can be used to pack workers in bundles:
```js
const WorkerPlugin = require('worker-plugin');
//...
module.exports = {
  //...
  plugins: [
    new WorkerPlugin({ sharedWorker: true })
  ]
}
//...
```

See [tests](./test) for further in-code examples.
