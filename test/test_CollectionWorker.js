// @ts-check
import './karma_index';

import prom from '@cern/prom';
import { afterEach, describe, it } from 'mocha';
import { expect } from 'chai';
import _ from "lodash";
import CollectionWorker from './index.worker';
import SharedCollectionWorker from './index.shared-worker';

/*::
declare var serverRequire: (string) => any
*/

describe('BaseCollectionWorker', function() {
  /** @type {Worker} */
  let worker;
  /** @type {SharedWorker} */
  let shared;

  afterEach(function() {
    if (worker) {
      worker.terminate();
      // @ts-ignore
      worker = null;
    }
    if (shared) {
      shared.port.close();
      // @ts-ignore
      shared = null;
    }
  });

  /**
   * @param  {Worker|MessagePort} worker
   * @param  {number=} timeout
   * @return {Promise<any>}
   */
  function waitResult(worker, timeout) {
    var defer = prom.makeDeferred();
    worker.onmessage = (e) => {
      const data = _.get(e, 'data');
      if (data && data.data) {
        defer.resolve(data);
      }
    };
    return prom.timeout(defer, timeout || 1000);
  }

  /**
   * @param  {Worker|MessagePort} worker
   * @param  {any} message
   * @return {Promise<any>}
   */
  async function waitWork(worker, message) {
    const prom = waitResult(worker);

    worker.postMessage(message);
    return await prom;
  }

  it('can filter data', (done) => {
    worker = new CollectionWorker();
    worker.onmessage = function(e) {
      const data /*: any */ = _.get(e, 'data');
      if (data && data.data) {
        expect(data.size).to.equal(2);
        expect(data.data).to.deep.equal([
          { user: 'carol', age: 44 },
          { user: 'bob', age: 21 }
        ]);
        expect(data.progress).to.equal(1);
        worker.postMessage({ destroy: true });
        done();
      }
    };
    worker.postMessage({
      data: [ { user: 'bob', age: 21 }, { user: 'carol', age: 44 },
        { weird: 'item' } ],
      orderBy: 'user:desc',
      // not really useful to escape, just there for the example
      search: { user: new RegExp(_.escapeRegExp('o')) }
    });
  });

  it('can update the request', () => {
    worker = new CollectionWorker();

    return Promise.resolve()
    .then(() => waitWork(worker, {
      data: [
        { user: 'bob', age: 21 }, { user: 'carol', age: 50 },
        { user: 'raoul', age: 44 }
      ]
    }))
    .then((ret) => expect(_.map(ret.data, 'user')).to.deep.equal(
      [ 'bob', 'carol', 'raoul' ]))
    .then(() => waitWork(worker, { orderBy: 'age:asc' }))
    .then((ret) => expect(_.map(ret.data, 'user')).to.deep.equal(
      [ 'bob', 'raoul', 'carol' ]))
    .then(() => waitWork(worker, { orderBy: 'age:desc',
      search: { age: { "$lt": 50 } } }))
    .then((ret) => expect(_.map(ret.data, 'user')).to.deep.equal(
      [ 'raoul', 'bob' ]))
    .finally(() => worker.postMessage({ destroy: true }));
  });

  it('can use a SharedWorker', () => {
    shared = new SharedCollectionWorker();
    shared.port.start();

    return Promise.resolve()
    .then(() => waitWork(shared.port, {
      data: [
        { user: 'bob', age: 21 }, { user: 'carol', age: 50 },
        { user: 'raoul', age: 44 }
      ]
    }))
    .then((ret) => expect(_.map(ret.data, 'user')).to.deep.equal(
      [ 'bob', 'carol', 'raoul' ]))
    .finally(() => shared.port.postMessage({ destroy: true }));
  });

  it('can use SharedWorkers in parallel', () => {
    const data = _.times(1000, (i) => ({ user: 'bob', age: i }));
    shared = new SharedCollectionWorker();
    var shared2 = new SharedCollectionWorker();
    shared.port.start();
    shared2.port.start();

    return Promise.resolve()
    .then(() => {
      shared.port.postMessage({ data, search: _.set({}, 'age.$gte', 250) });
      shared2.port.postMessage({ data, search: _.set({}, 'age.$lt', 250) });
    })
    .then(_.constant(waitResult(shared.port)))
    .then((ret) => expect(ret.size).to.equal(750))
    .then(_.constant(waitResult(shared2.port)))
    .then((ret) => expect(ret.size).to.equal(250))
    .finally(() => {
      shared.port.postMessage({ destroy: true });
      shared2.port.postMessage({ destroy: true });
      shared2.port.close();
    });
  });
});
