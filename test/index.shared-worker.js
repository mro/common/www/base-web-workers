// @ts-check

import { BaseCollectionWorker } from '../src';
import { has, set } from 'lodash';

class CustomSharedWorker extends BaseCollectionWorker {
  /**
   * @param {BaseCollectionWorker.MessageIn} msg
   * @return {void}
   */
  process(msg) {
    if (has(msg, 'hello')) {
      this.post({ world: true });
    }
    super.process(msg);
  }
}

set(BaseCollectionWorker, 'create',
  (/** @type {BaseCollectionWorker.PostFunction} */ post) => new CustomSharedWorker(post));
