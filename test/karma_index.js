// @ts-check

/* must be done early */
// @ts-ignore
require('debug').useColors = () => false; /* eslint-disable-line global-require */

const
  { before } = require('mocha'),
  server = require('@cern/karma-server-side'),
  chai = require('chai'),
  dirtyChai = require('dirty-chai');

chai.use(dirtyChai);

before(function() {
  return server.run(function() {
    return process.env['DEBUG'];
  })
  .then((/** @type {any} */ dbg) => {
    // @ts-ignore
    localStorage['debug'] = dbg;
  });
});
