import { expectType } from 'tsd';

import { BaseCollectionWorker } from '.';

(async function() {
  const worker = new BaseCollectionWorker((post) => {
    expectType<number|undefined>(post.size);
  });

  worker.post({ data: 42, progress: 12 });
  worker.abort();
  worker.destroy();
  worker.sendResult();
  worker.process({ data: 42 });
}());
