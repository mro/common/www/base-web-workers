// @ts-check

import {
  assign, defaultTo, get, has, noop, orderBy, size, split } from 'lodash';
import { mfilterCtrl } from '@cern/mfilter';

const ProgressDelta = 0.05;

/** @type {typeof BaseWorker.BaseCollectionWorker.Flags} */
const Flags = {
  ORDER: 0x01,
  SEARCH: 0x02,
  DESTROYED: 0x04
};

class CollectionWorker {
  /**
   * @param {BaseWorker.BaseCollectionWorker.PostFunction} post
   */
  constructor(post) {
    this.post = post;
    /** @type {BaseWorker.BaseCollectionWorker.MessageIn & { offset: number, limit: number }} */
    this.in = { limit: 10, offset: 0 };

    /** @type {BaseWorker.BaseCollectionWorker.MessageOut} */
    this.out = {};

    /** @type {BaseWorker.BaseCollectionWorker.Flags} */
    this.flags = 0;

    this._lastProgressEvent = 0;

    /** @type {?any[]} */
    this.filtered = null;

    /** @type {?() => any} */
    this._abort = null;
  }

  abort() {
    if (this._abort) {
      this._abort();
      this._abort = null;
    }
  }

  destroy() {
    this.abort();
    this.post = noop;
    this.in.data = null;
    this.filtered = null;
    this.flags |= Flags.DESTROYED;
  }

  sendResult() {
    this.out.progress = 1;
    if (this.filtered) {
      this.out.data = this.filtered.slice(this.in.offset,
        this.in.offset + this.in.limit);
      // $FlowIgnore
      this.out.size = this.filtered.length;
    }
    else {
      this.out.data = [];
      this.out.size = 0;
    }
    this.post(this.out);
  }

  /**
   * @param {number} inc
   */
  _incProgress(inc) {
    this.out.progress = (this.out.progress || 0) + inc;
    if (Math.abs(this._lastProgressEvent - this.out.progress) > ProgressDelta) {
      this.post(this.out);
      this._lastProgressEvent = this.out.progress || 0;
    }
  }

  /**
   * @param {BaseWorker.BaseCollectionWorker.MessageIn} e
   */
  onmessage(e) {
    const data = get(e, 'data');
    if (this.flags & Flags.DESTROYED) {
      console.log('[CollectionWorker]: message received after destruction');
    }
    else if (has(data, 'destroy')) {
      this.destroy();
    }
    else {
      this.process(data);
    }
  }

  /**
   * @param {BaseWorker.BaseCollectionWorker.MessageIn} msg
   */
  _updateIn(msg) {
    this.flags |= (msg.search && (msg.search !== this.in.search)) ?
      (Flags.SEARCH | Flags.ORDER) : 0;
    this.flags |= (msg.orderBy && (msg.orderBy !== this.in.orderBy)) ?
      Flags.ORDER : 0;
    assign(this.in, msg);
  }

  /**
   * @param {BaseWorker.BaseCollectionWorker.MessageIn} msg
   */
  process(msg) {
    if (!msg) { return; }

    this.out.inputSize = size(this.in.data);

    this.abort();
    this._updateIn(msg);

    if (msg.data) {
      this.flags |= (Flags.ORDER | Flags.SEARCH);
      this.filtered = null;
    }
    else if (!this.in.data) {
      return;
    }

    if (!this.flags) {
      this.sendResult();
    }
    else {
      /** @type {BaseWorker.BaseCollectionWorker.Context} */
      var ctx /*: any */ = { pending: true };
      Promise.resolve()
      .then(() => {
        this.out.progress = 0;
        this._lastProgressEvent = 0;
        delete this.out.data;
        this.post(this.out);
      })
      .then(() => {
        /* filter */
        if (!(this.flags & Flags.SEARCH) || !ctx.pending) {
          return this.filtered;
        }
        else if (!this.in.search) {
          this._incProgress(0.9);
          return this.in.data;
        }
        else {
          ctx.filter = mfilterCtrl(this.in.data, this.in.search,
            this._incProgress.bind(this, 0.9 / (this.out.inputSize || 1)));
          return ctx.filter.promise;
        }
      })
      .then((filtered) => {
        this.filtered = filtered;
        this.flags &= ~Flags.SEARCH;
      })
      .then(() => {
        /* sort */
        if (!(this.flags & Flags.ORDER) || !ctx.pending || !this.in.orderBy) {
          return;
        }
        const crit = split(this.in.orderBy, ':');
        this.filtered = orderBy(this.filtered, crit[0],
          // @ts-ignore: should be asc/desc
          defaultTo(crit[1], 'asc'));
        this.flags &= ~Flags.ORDER;
      })
      .then(() => {
        if (!ctx.pending) { return; }
        this.sendResult();
      })
      .catch(noop);

      this._abort = () => {
        ctx.pending = false;
        if (ctx.filter) { ctx.filter.abort(); }
      };
    }
  }
}

/** @type {(post: BaseWorker.BaseCollectionWorker.PostFunction) => CollectionWorker} */
CollectionWorker.create = (post) => new CollectionWorker(post);

if (has(self, 'onmessage')) { // dedicated worker
  const ctx = /** @type {DedicatedWorkerGlobalScope} */ (/** @type {unknown} */ (self));

  /** @type {?CollectionWorker} */
  var worker;
  ctx.onmessage = function(m) {
    worker = CollectionWorker.create((m) => ctx.postMessage(m));
    ctx.onmessage = worker.onmessage.bind(worker);
    worker.onmessage(m);
  };
}
else if (has(self, 'onconnect')) { // shared worker
  const ctx = /** @type {SharedWorkerGlobalScope} */ (/** @type {unknown} */ (self));

  ctx.onconnect = function(e) { // jshint ignore:line
    const port = e.ports[0];
    const worker = CollectionWorker.create((m) => port.postMessage(m));
    port.onmessage = worker.onmessage.bind(worker);
    port.start();
  };
}
else {
  console.log('unknown worker type');
}

export default CollectionWorker;
