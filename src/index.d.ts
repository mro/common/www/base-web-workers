
import mfilter from '@cern/mfilter';

export = BaseWorker;
export as namespace BaseWorker;

declare namespace BaseWorker {
  // not sure why this is necessary :s
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  export class BaseCollectionWorker {
    constructor(post: BaseCollectionWorker.PostFunction);

    in: BaseCollectionWorker.MessageIn;

    out: BaseCollectionWorker.MessageOut;

    post: BaseCollectionWorker.PostFunction;

    flags: BaseCollectionWorker.Flags;

    filtered: any[]|null;

    abort(): void;

    destroy(): void;

    sendResult(): void;

    onmessage(message: any): void;

    process(message: BaseCollectionWorker.MessageIn): void;
  }
  namespace BaseCollectionWorker {
    export var create: (post: PostFunction) => BaseCollectionWorker;

    type PostFunction = (message:
      BaseCollectionWorker.MessageOut & { [key: string]: any }) => any

    interface MessageIn {
      data?: any;
      orderBy?: string; // suffix with :asc/:desc
      limit?: number;
      search?: { [key: string]: string };
      offset?: number;
      destroy?: any; // close notification
    }

    interface MessageOut {
      progress?: number;
      data?: any;
      inputSize?: number;
      size?: number;
    }

    enum Flags {
      ORDER, SEARCH, DESTROYED
    }

    interface Context {
      pending: boolean;
      filter?: mfilter.Control;
    }
  }
}
