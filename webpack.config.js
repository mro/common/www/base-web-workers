const
  path = require('path'),
  child = require('child_process'),
  webpack = require('webpack'),
  _ = require('lodash'),
  { execSync } = require('child_process');

module.exports = {
  mode: 'development',
  entry: path.resolve('./src/index.js'),
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'index.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          extends: path.join(__dirname, '.babelrc')
        }
      },
      {
        test: /\.shared-worker\.js/,
        loader: 'worker-loader',
        options: {
          filename: '[name].js',
          worker: {
            type: 'SharedWorker',
            options: { name: 'worker' }
          }
        }
      },
      {
        test: /\.worker\.js/,
        loader: 'worker-loader',
        options: {
          filename: '[name].js',
          worker: {
            type: 'Worker',
            options: { name: 'worker' }
          }
        }
      }
    ]
  },
  resolve: {
    extensions: [ '.js', '.json' ]
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: "warning"
  },
  devtool: 'inline-cheap-module-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      Buffer: [ 'buffer', 'Buffer' ]
    }),
    new webpack.DefinePlugin({
      VERSION: JSON.stringify(child.execSync('git describe --tags --always HEAD').toString().trim())
    }),
    {
      apply: (compiler) => {
        compiler.hooks.beforeCompile.tap('IstanbulPatch', () => {
          /* see https://github.com/istanbuljs/nyc/issues/718 for details */
          execSync("sed -i='tmp' 's/source: pathutils.relativeTo(start.source, origFile),/source: origFile,/' node_modules/istanbul-lib-source-maps/lib/get-mapping.js")
        });
      }
    }
  ]
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = 'source-map'
  module.exports.mode = 'production'
  _.set(module.exports, 'optimization.nodeEnv', 'production');
  _.set(module.exports, 'optimization.moduleIds', 'named');
}
